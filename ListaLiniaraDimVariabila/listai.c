#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lista.h"

LISTA newl()
{
    LISTA l =(LISTA)malloc(sizeof(Lista));
    if(l==NULL)
        return NULL;
    l->nmax=NMAX; // a reusit alocarea header-ului listei
    l->inc=INCREMENT;
    l->liber=0;
    if((l->v= (DATA *)malloc( l->nmax * sizeof(DATA) ) ) ==NULL)
    {
        free(l); // a esuat alocarea vectorului
        l=NULL; // ce contine elem. listei
    }
    return l;
}
//-------------------------------------
// Distruge lista
//
void destroyl(LISTA l)
{
    if(l!=NULL)
    {
        free(l->v); // elib. mem. ocupata de vector
        free(l); // elib. mem. ocupata de header lista
    }
}
//---------------------------------------
// Inserarea unui nou element
// la sfarsitul listei
// Daca lista este plina, extinde dimensiunea vectorului
//
LISTA ins_la_urma(LISTA l, DATA x)
{
    if(isFull(l))
    {
        DATA *w;
        if((w=(DATA *)realloc(l->v, (l->nmax + l->inc)*sizeof(DATA) ) )==NULL)
            return l;
        l->nmax += l->inc; // actualizare dim. vector
        l->v = w; // in header pointer catre vectorul nou (extins)
    }
    l->v[l->liber++]=x; // se insereaza elementul x
    return l;
}
//
// Sterge ultimul element introdus
//
LISTA sterge_ultim(LISTA l)
{
    if(!isEmptyl(l))
        l->liber--;
    return l;
}
//
//-------------------------------------
// Returneaza valoarea primului element
// al listei
//
DATA primul(LISTA l)
{
    return isEmptyl(l) ? ABSENT : l->v[0];
}
//-------------------------------------
bool isEmptyl(LISTA l)
{
    return l==NULL || l->liber==0;
}
//-------------------------------------
bool isFull(LISTA l)
{
    return l!=NULL && l->liber==l->nmax;
}
//
//-------------------------------------
// Conversie lista -> sir
//
char *toStringl(LISTA l, char *s)
{
    int i;
    char zona[50];
    strcpy(s,"{");
    if(isEmptyl(l))
        strcat(s,"<vida>}");
    else
        for(i=0; i<l->liber; i++)
        {
            sprintf(zona,FORMAT"%c",l->v[i], i==l->liber-1?'}':',');
            strcat(s,zona);
        }
    return s;
}
//
// -----------------------------------
// Cautarea elementului cu informatia k;
// Functia returneaza pozitia pe care a fost gasit
// sau valoarea -1 daca nu a fost gasit elementul
int cauta_elem(LISTA l, DATA k)
{
    if (isEmptyl(l)) return -1;
    for (int i=0;i<l->liber;i++)
        if (l->v[i] == k)
            return i;
    return -1;
}

//-----------------------------------
LISTA ins_la_inceput(LISTA l, DATA x)
{
	if (isFull(l))
		return l;
    if(isEmptyl(l))
        return ins_la_urma(l, x);
    if (!isFull(l))
    {
        l->liber++;// incrementam dimensiunea listei cu 1, adica elementul ce va fi inserat
        for (int i=l->liber;i>0; i--)
            l->v[i] = l->v[i-1];
        l->v[0] = x;
    }
    return l;
}
//----------------------------------
LISTA populeaza_lista_din_fisier(LISTA l, char* numeFisier)
{
    FILE* f = fopen(numeFisier, "r");
    if (f == NULL)
    {
        printf("\nEroare la deschiderea fisierului. ");
        return l;
    }
    int aux;
    while(fscanf(f, "%d ", &aux) > 0)
        l = ins_la_urma(l, aux);
	fclose(f);
    return l;
}
//------------------------------------------------
LISTA ins_dupa_un_anumit_element(LISTA l, int elementDeInserat, int elementPrecedent)
{
    if (isEmptyl(l)) return l;
    if(isFull(l))
    {
        DATA *w;
        if((w=(DATA *)realloc(l->v, (l->nmax + l->inc)*sizeof(DATA) ) )==NULL)
            return l;
        l->nmax += l->inc; // actualizare dim. vector
        l->v = w; // in header pointer catre vectorul nou (extins)
    }
    for (int i=l->liber; i>0; i--)
    {
        if (l->v[i-1] == elementPrecedent)
        {
            l->liber++;
            for (int j=l->liber-1; j!=i; j--)
                l->v[j] = l->v[j-1];
            l->v[i] = elementDeInserat;
            return l;
        }
    }
    printf("Elementul "FORMAT" nu este in lista.", elementPrecedent);
    return l;
}
//-------------------------------------------
LISTA elimina_primul(LISTA l)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida!");
        return l;
    }
    for (int i=0; i<l->liber-1; i++)
        l->v[i] = l->v[i+1];
    l->liber--;
    return l;
}
//------------------------------------------
void salveaza_lista_in_fisier(LISTA l, char* numeFisier)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida, nu vom realiza scrierea in fisier.");
        return;
    }
    FILE* f = fopen(numeFisier, "aw");
    if (f == NULL)
    {
        printf("\nEroare la scrierea in fisier.");
        return;
    }
    for (int i=0;i<l->liber;i++)
        fprintf(f, "%d ", l->v[i]);
    fclose(f);
}
//-----------------------------------------
void formeazaListe(LISTA l, LISTA *lpoz, LISTA *lneg)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida. Operatie imposibila!");
        return;
    }
    for (int i=0;i<l->liber;i++)
        if (l->v[i] >= 0)
            *lpoz=ins_la_urma(*lpoz, l->v[i]);
        else
            *lneg=ins_la_urma(*lneg, l->v[i]);
}
//-----------------------------------------
LISTA elimina_x(LISTA l, DATA x)
{
    if (isEmptyl(l)) return l;
    for (int i=0;i<l->liber;i++)
        if (l->v[i] == x)
        {
            for (int j=i; j<l->liber-1;j++)
                l->v[j] = l->v[j+1];
            l->liber--;
            return l;
        }
    printf("\nElementul "FORMAT" nu exista in lista.", x);
    return l;
}
