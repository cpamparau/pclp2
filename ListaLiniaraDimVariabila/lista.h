#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <stdbool.h>
#include "data.h"
//
//----------------------------------
// Proprietatile TDA
//
struct dlist
{
    int nmax; // dimensiunea actuala a vectorului (initial =NMAX)
    int liber; // indicele primului element liber din vector(initial =0)
    int inc; // incrementul cu care este extins vectorul
    DATA *v; // pointer catre vector
};
typedef struct dlist Lista;
typedef struct dlist *LISTA;
//
//----------------------------------
// Metodele (operatiile) TDA
//
LISTA newl();
LISTA ins_la_urma(LISTA l, DATA x);
LISTA sterge_ultim(LISTA l);
DATA primul(LISTA l);
bool isEmptyl(LISTA l);
bool isFull(LISTA l);
char *toStringl(LISTA l,char *s);
void destroyl(LISTA l);
// ...
int cauta_elem(LISTA l, DATA k);

#endif // LISTA_H_INCLUDED
