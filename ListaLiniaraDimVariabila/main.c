#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "lista.h"

int meniu();
void infoProgram();

int main()
{
    LISTA l = newl();
    LISTA lpoz = newl();
    LISTA lneg = newl();
    char buffer[1024];
    char numeFisier[200];
    while(ADEVARAT)
    {
        system("cls");
        switch(meniu())
        {
            case 1:
                printf("\nIntroduceti numarul intreg ce va fi adaugat la inceputul listei: ");
                int numberToBeAdd;
                scanf("%d", &numberToBeAdd);
                int beforeAdd = l->liber;
                l = ins_la_inceput(l, numberToBeAdd);
                int afterAdd = l->liber;
                if (afterAdd>beforeAdd)
                    printf("\nAdaugarea s-a efectuat cu succes!");
                else
                    printf("\nAdaugare esuata!");
                break;
            case 2:
                printf("\nIntroduceti numele fisierului de intrare: ");
                fflush(stdin);
                gets(numeFisier);
                int liberBefore = l->liber;
                l = populeaza_lista_din_fisier(l, numeFisier);
                int liberAfter = l->liber;
                if (liberAfter>liberBefore)
                    printf("\nLista a fost actualizata cu datele din fisier.");
                else
                    printf("\nAdaugarea elementelor din fisier a esuat.");
                break;
            case 3:
                printf("%s", toStringl(l,buffer));
                break;
            case 4:
                printf("\nIntroduceti elementul DE INTRODUS: ");
                int elementDeIntrodus;
                scanf("%d", &elementDeIntrodus);
                printf("\nIntroduceti valoarea elementului DUPA care se va introduce elementul %d: ",elementDeIntrodus);
                int elementInfo;
                scanf("%d", &elementInfo);
                beforeAdd = l->liber;
                l = ins_dupa_un_anumit_element(l, elementDeIntrodus,elementInfo);
                afterAdd = l->liber;
                if (afterAdd>beforeAdd)
                    printf("\nAdaugarea s-a efectuat cu succes!");
                else
                    printf("\nAdaugare esuata!");
                break;
            case 5:
                printf("\nIntroduceti elementul cautat: ");
                int elem;
                scanf("%d", &elem);
                int poz = cauta_elem(l, elem);
                if ( poz == -1)
                    printf("\nElementul "FORMAT" nu exista in lista.", elem);
                else
                    printf("\nElementul se afla in lista pe pozitia ."FORMAT, poz);
                break;
            case 6:
                printf("\nEliminam primul element din lista.");
                int beforeDelete = l->liber;
                l = elimina_primul(l);
                int afterDelete = l->liber;
                if (afterDelete < beforeDelete)
                    printf("\nStergerea s-a efectuat cu succes!");
                else
                    printf("\nStergere esuata!");
                break;
            case 7:
                printf("\nEliminam ultimul element din lista.");
                beforeDelete = l->liber;
                l = sterge_ultim(l);
                afterDelete = l->liber;
                if (afterDelete < beforeDelete)
                    printf("\nStergerea s-a efectuat cu succes!");
                else
                    printf("\nStergere esuata!");
                break;
            case 8:
                printf("\nIntroduceti elementul de sters: ");
                int em;
                scanf("%d", &em);
                elimina_x(l, em);
                break;
            case 9:
                printf("\nIntroduceti numele fisierului: ");
                fflush(stdin);
                gets(numeFisier);
                salveaza_lista_in_fisier(l, numeFisier);
                printf("\nScriere cu succes!");
                break;
            case 10:
                int lpozLiber = lpoz->liber, lnegLiber = lneg->liber;
                formeazaListe(l, &lpoz, &lneg);
                if (lpoz->liber > lpozLiber || lneg->liber > lnegLiber)
                    printf("\nOperatie efectuata cu succes.");
                else
                    printf("\nOperatie esuata.");
                break;
            case 11:
                printf("%s", toStringl(lpoz,buffer));
                break;
            case 12:
                printf("%s", toStringl(lneg,buffer));
                break;
            case 13:
                infoProgram();
                break;
            case 14:
                return 0;
            default:
                printf("\nOptiune inexistenta!");
                break;
        }
        getch();
    }
    destroyl(l);
    destroyl(lpoz);
    destroyl(lneg);
    return 0;
}

int meniu()
{
    printf("\n1 - citeste un numar intreg si-l introduce la inceputul listei");
    printf("\n2 - citeste un fisier text ce contine numere intregi separate prin spatii si le introduce in lista (la sfarsitul listei)");
    printf("\n3 - afiseaza lista");
    printf("\n4 - insereaza un element in lista dupa un element cu informatia data");
    printf("\n5 - cauta un element �n lista");
    printf("\n6 - elimina primul element din lista");
    printf("\n7 - elimina ultimul element din lista");
    printf("\n8 - elimina din lista elementul cu informatia x");
    printf("\n9 - salveaza lista intr-un fisier text");
    printf("\n10 - din lista L se formeaza doua noi liste, una ce contine elementele negative si una continand elementele pozitive");
    printf("\n11 - afiseaza lista continand elementele pozitive");
    printf("\n12 - afiseaza lista continand elementele negative");
    printf("\n13 - informatii despre autor");
    printf("\n14 - terminare program");

    printf ("\n\nPrecizeaza optiunea: ");
	fflush(stdin);
	int optiune;
	scanf("%d", &optiune);
	return optiune;
}

void infoProgram()
{
    printf("\nProgram demonstrativ cu lista liniara de dimensiune variabila.");
}
