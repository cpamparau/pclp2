
//#include <stdio.h>
//#include <stdlib.h>
//#include <conio.h>

#include "studenti.h"

char meniu();
void infoProgram();

int main()
{
    // se declara o variabila de tip STUDENT
    // tipul STUDENT este definit in studenti.h
    STUDENT s;
    char numeFisier[200];

    // constantele ADEVARAT si FALS sunt definite in studenti.h
	while(ADEVARAT){
		system("cls");
		switch(meniu()){
			case 'C': // se citeste un student de la tastatura
                citireStudent(&s);
				break;
			case 'A': // se afiseaza studentul pe ecran
                afisareStudent(s);
				break;
			case 'P': // se adauga studentul in fisier
                 printf("\nDa numele fisierului in care se va adauga studentul:");
				 fflush(stdin);
				 gets(numeFisier);
				 if (adaugareStudentInFisier(numeFisier,s)==FALS)
					printf ("\nEroare la adaugarea studentului.");
				 else
					printf ("\nAdaugarea studentului s-a realizat cu succes.");
				break;
			case 'V': // se afiseaza toti studentii din fisier (in mod binar)
                 printf("\nDa numele fisierului in care se afla studentii:");
				 fflush(stdin);
				 gets(numeFisier);
				 if (vizualizareStudentiDinFisierModBinar(numeFisier)==FALS)
					printf ("\nEroare la afisarea datelor din fisier.");
				 else
					printf ("\nAfisarea datelor din fisier s-a incheiat cu succes.");
				break;
            case 'Y': // se afiseaza toti studentii din fisier (in mod text)
                 printf("\nDa numele fisierului in care se afla studentii:");
				 fflush(stdin);
				 gets(numeFisier);
				 if (vizualizareStudentiDinFisierModText(numeFisier)==FALS)
					printf ("\nEroare la afisarea datelor din fisier.");
				 else
					printf ("\nAfisarea datelor din fisier s-a incheiat cu succes.");
				break;
            case 'S': // se cauta in fisier un student dupa numarul de inmatriculare citit de la tastatura
                printf("\nIntroduceti numele fisierului din care se face cautarea:");
                fflush(stdin);
                gets(numeFisier);
                printf("\nIntroduceti numarul matricol al studentului care se cauta:");
                int nrMatricol;
                scanf("%d", &nrMatricol);
                LOGIC *errFisier;
                int pozitie = gasitStudentModText(numeFisier,nrMatricol,errFisier);
                if (pozitie == -1)
                    printf("\nStudentul cu numarul matricol %d NU EXISTA in fisier.", nrMatricol);
                else
                    printf("\nStudentul se afla in fisier pe pozitia %d\n", pozitie);
                break;
            case 'B': // se cauta in fisier un student dupa numarul de inmatriculare citit de la tastatura
                printf("\nIntroduceti numele fisierului din care se face cautarea:");
                fflush(stdin);
                gets(numeFisier);
                printf("\nIntroduceti numarul matricol al studentului care se cauta:");
                scanf("%d", &nrMatricol);
                pozitie = gasitStudentModBinar(numeFisier,nrMatricol,errFisier);
                if (pozitie == -1)
                    printf("\nStudentul cu numarul matricol %d NU EXISTA in fisier.", nrMatricol);
                else
                    printf("\nStudentul se afla in fisier pe pozitia %d\n", pozitie);
                break;
			case 'I':infoProgram();
				break;
			case 'X':return 0;
			default:printf("Optiune inexistanta");
		}
		getch();
	}
    return 0;
}

char meniu(){
	printf ("\nC - citire student de la tastatura");
	printf ("\nA - afisare student pe ecran");
	printf ("\nP - adaugare student in fisier");
	printf ("\nV - vizualizare studenti din fisier (in mod binar)");
	printf ("\nY - vizualizare studenti din fisier (in mod text)");
	printf ("\nS - cauta student in fisier dupa numar matricol (in mod text)");
	printf ("\nB - cauta student in fisier dupa numar matricol (in mod binar)");
	printf ("\nI - info program");
	printf ("\nX - parasire program");

	printf ("\n\nPrecizeaza optiunea:");
	fflush(stdin);
	return toupper(getche());
}

void infoProgram(){
	printf ("\nProgram demonstrativ cu structuri.");
}

