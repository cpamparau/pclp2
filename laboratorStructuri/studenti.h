// protectie impotriva incluziunii multiple
// a acestui fisier header
#ifndef STUDENTI_H_INCLUDED
#define STUDENTI_H_INCLUDED

#include <stdio.h>

// definirea tipului de data LOGIC
// si a constrantelor ADEVARAT si FALS
typedef char LOGIC;
#define ADEVARAT 1
#define FALS 0

// definirea tipului de data STUDENT
struct student{
	long  nrMatricol;
	char  nume[100];
	float media;
};
typedef struct student STUDENT;  // typedef creaza sinonimul STUDENT
                                 // pentru denumirea struct student

// prototipul functiilor utilizate
void citireStudent(STUDENT* ps);
void afisareStudent(STUDENT s);
LOGIC adaugareStudentInFisier(char* numeFisier, STUDENT s);
LOGIC vizualizareStudentiDinFisierModBinar(char* numeFisier);
LOGIC vizualizareStudentiDinFisierModText(char* numeFisier);
int gasitStudentModBinar(char* numeFisier, long nrMatricol, LOGIC* errFisier);
int gasitStudentModText(char* numeFisier, long nrMatricol, LOGIC* errFisier);

#endif // STUDENTI_H_INCLUDED

