//#include <ctype.h>
//#include <conio.h>

#include "studenti.h"
void citireStudent(STUDENT* ps){
    // functia primeste un pointer la o variabila de tip STUDENT
    // si de aceea accesul la campuri se face cu operatorul sageata (->)
    printf ("\nIntroduceti pe rand nr. matricol, numele si media:\n");
	scanf ("%ld",&ps->nrMatricol);
	fflush(stdin);
	gets (ps->nume);
	scanf ("%f",&ps->media);
}

void afisareStudent(STUDENT s){
    // functia primeste o variabila de tip STUDENT
    // si de aceea accesul la campuri se face cu operatorul punct (.)
    printf ("\n%10ld\t|%50s\t|%4.2f",s.nrMatricol,s.nume,s.media);
}

// tipul de data LOGIC este definit in student.h
LOGIC adaugareStudentInFisier(char* numeFisier, STUDENT s){
	// se deschide fisierul in mod adaugare-binar
	FILE *fp = fopen (numeFisier, "ab");

	//daca fisierul nu poate fi deschis, se returneaza fals logic
	if (fp == NULL)	return FALS;
	// daca fp este un pointer valid atunci se pot scrie datele
	// functia fwrite primeste 4 parametrii:
        // &s - adresa unde se afla datele ce trebuie scrise in fisier
        // sizeof(STUDENT) - numarul de octeti ocupati de o variabila STUDENT (4+100+4=108)
        // 1 - numarul de studenti ce sunt adaugati
        // fp - pointerul le fisier
	fwrite (&s, sizeof(STUDENT), 1, fp);
	fclose (fp);
	return ADEVARAT;
}

LOGIC vizualizareStudentiDinFisierModBinar(char* numeFisier){
	STUDENT s;
	// se deschide fisierul in mod citire-binar
	FILE *fp = fopen (numeFisier, "rb");

	//daca fisierul nu poate fi deschis, se returneaza fals logic
	if (fp == NULL)	return FALS;
	// functia fread primeste aceeasi parametrii ca si functia fwrite
	// returneaza 0 atunci cand a ajuns la sfarsitul fisierului
	// se citesc si se afiseaza pe rand toti studentii din fisier
	while (fread(&s, sizeof(STUDENT), 1, fp)>0)
        afisareStudent(s);


	fclose (fp);
	return ADEVARAT;
}

LOGIC vizualizareStudentiDinFisierModText(char* numeFisier)
{
    STUDENT s;
    FILE* f = fopen(numeFisier, "r");
    if (f == NULL)
    {
        return -1;
    }

    int nrStudenti;
    fscanf(f, "%d\n", &nrStudenti);
    printf("nrStudenti=%d\n",nrStudenti);

    for(int i=0;i<nrStudenti;i++)
    {
//        char *code = malloc(1000);
//        size_t n = 0;
//        int c;
//        while ((c = fgetc(f)) != '\n')
//        {
//            code[n++] = (char) c;
//        }
//        code[n] = '\0';
//        // primul parametru este numarul Matricol, urmat de spatiu
//        char* token = strtok(code, " ");
//        s.nrMatricol = atoi(token);
//        if (token != NULL)
//            token = strtok(NULL, ",");
//        strcpy(s.nume, token);
//        if(token != NULL)
//            token = strtok(NULL, "\n");
//        s.media = (float)atof(token);
        fscanf(f, "%ld %[^,]%*c %f", &s.nrMatricol, &s.nume, &s.media);
        afisareStudent(s);
    }
}

int gasitStudentModBinar(char* numeFisier, long nrMatricol, LOGIC* errFisier)
{
    FILE* f = fopen(numeFisier, "rb");
    if (f == NULL)
    {
        *errFisier = FALS;
        printf("Eroare la deschiderea fisierului!\n");
        return -1;
    }
//    STUDENT* studenti = NULL;
//    int count=0;
//    STUDENT aux;
//	while (fread(&aux, sizeof(STUDENT), 1, f)>0)
//    {
//        studenti = (STUDENT*) realloc(studenti, sizeof(STUDENT)* (count+1));
//        studenti[count] = aux;
//        afisareStudent(studenti[count]);
//        count++;
//    } // i a fost incrementat anterior la fiecare operatie corecta
//    printf("count=%d\n", count);
//    for (int i=0;i<count;i++)
//    {
//        if (studenti[i].nrMatricol == nrMatricol)
//            return i;
//    }
    STUDENT aux;
    int count = 0;
    while(fread(&aux, sizeof(STUDENT), 1, f) > 0)
    {
        if (aux.nrMatricol == nrMatricol)
        {
            fclose(f);
            return count;
        }
        count++;
    }
    fclose(f);
    return -1;
}

int gasitStudentModText(char* numeFisier, long nrMatricol, LOGIC* errFisier)
{
    FILE* f = fopen(numeFisier, "r");
    if (f == NULL)
    {
        return -1;
    }
    // citim prima linie, care ne indica numarul de studenti
    int nrStudenti;
    fscanf(f, "%d\n", &nrStudenti);
    printf("nrStudenti=%d\n",nrStudenti);

    // odata ce stim numarul de studenti, vom crea un vector de Studenti
    // cu dimensiunea citita in fisier si salvata in variabila nrStudenti
    STUDENT* studenti = (STUDENT*)malloc(sizeof(STUDENT) * nrStudenti);
    for(int i=0;i<nrStudenti;i++)
    {
//        char *code = malloc(1000);
//        size_t n = 0;
//        int c;
//        while ((c = fgetc(f)) != '\n')
//        {
//            code[n++] = (char) c;
//        }
//        code[n] = '\0';
//        // primul parametru este numarul Matricol, urmat de spatiu
//        char* token = strtok(code, " ");
//        studenti[i].nrMatricol = atoi(token);
//        if (token != NULL)
//            token = strtok(NULL, ",");
//
//        strcpy(studenti[i].nume, token);
//        if(token != NULL)
//            token = strtok(NULL, ",");
//        studenti[i].media = (float)atof(token);
        fscanf(f, "%ld %[^,]%*c %f", &studenti[i].nrMatricol, &studenti[i].nume, &studenti[i].media);
        afisareStudent(studenti[i]);
    }

    // am salvat toti studentii in vectorul de studenti anterior
    // vom cauta in vector studentul cu numarul matricol nrMatricol
    for(int i=0;i<nrStudenti;i++)
    {
        if (studenti[i].nrMatricol == nrMatricol)
            return i;
    }
    fclose(f);
    return -1;
}
