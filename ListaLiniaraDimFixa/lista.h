#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <stdbool.h>
#include "data.h"
//
//----------------------------------
// Proprietatile TDA
//
struct lista
{
    int liber;
    DATA v[NMAX];
};
typedef struct lista Lista;
typedef struct lista *LISTA;
//
//----------------------------------
// Metodele (operatiile) TDA
//
LISTA newl();
LISTA ins_la_urma(LISTA l, DATA x);
LISTA sterge_ultim(LISTA l);
DATA primul(LISTA l);
bool isEmptyl(LISTA l);
bool isFull(LISTA l);
char *toStringl(LISTA l,char *s);
void destroyl(LISTA l);

//
//----------------------------------
// metode adaugate de programator
//
LISTA ins_la_inceput(LISTA l, DATA x);
LISTA populeaza_lista_din_fisier(LISTA l, char* numeFisier);
LISTA ins_dupa_un_anumit_element(LISTA l, int elementDeInserat, int elementPrecedent);
int cauta(LISTA l, DATA x);
LISTA elimina_primul(LISTA l);
void salveaza_lista_in_fisier(LISTA l, char* numeFisier);
void formeazaListe(LISTA l, LISTA* lpoz, LISTA* lneg);
LISTA elimina_x(LISTA l, DATA x);
#endif // LISTA_H_INCLUDED
