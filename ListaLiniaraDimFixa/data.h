#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED

typedef unsigned char LOGIC;
//
//-----------------------------------------
// Rezervat modificarilor utilizatorului
// care va preciza tipul informatiei din
// elementele TDA, precum si numarul maxim
// de elemente ale TDA (NMAX)
//-----------------------------------------
//
typedef int DATA;
#define FORMAT "%d"
#define ABSENT 0
#define NMAX 100
#define ADEVARAT 1
#define FALS 0
#define MAX_LENGTH_FILE 255

#endif // DATA_H_INCLUDED
