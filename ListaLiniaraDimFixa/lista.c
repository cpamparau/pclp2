#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lista.h"


// Initializare lista
//
LISTA newl()
{
    LISTA w=(LISTA)malloc(sizeof(Lista));
    if(w!=NULL)
        w->liber=0;
    return w;
}
//--------------------------
// Distruge lista
//
void destroyl(LISTA l)
{
    if(l!=NULL)
    {
        free(l); // elib. mem. ocupata de lista ( {liber,v[NMAX]} )
    }
}
//----------------------------------------------
// Inserarea unui nou element
// la sfarsitul listei
//
LISTA ins_la_urma(LISTA l, DATA x)
{
    if(!isFull(l))
        l->v[ l->liber++ ] = x;
    return l;
}
//-------------------------------------
// Sterge ultimul element introdus
//
LISTA sterge_ultim(LISTA l)
{
    if(!isEmptyl(l))
        l->liber--;
    return l;
}
//----------
// Returneaza valoarea primului element
// al listei
DATA primul(LISTA l)
{
    return isEmptyl(l) ? ABSENT : l->v[0];
}
//-------------------------------------
bool isEmptyl(LISTA l)
{
    return l==NULL || l->liber==0;
}
//-------------------------------------
bool isFull(LISTA l)
{
    return l!=NULL && l->liber==NMAX;
}
//
//-------------------------------------
// Conversie lista -> sir
//
char *toStringl(LISTA l, char *s)
{
    int i;
    char zona[10];
    strcpy(s,"{");
    if(isEmptyl(l))
        strcat(s,"<vida>}");
    else
        for(i=0; i<l->liber; i++)
        {
            sprintf(zona,FORMAT"%c", l->v[i], i==l->liber-1?'}':',');
            strcat(s,zona);
        }
    return s;
}
//-----------------------------------
LISTA ins_la_inceput(LISTA l, DATA x)
{
    if(isEmptyl(l))
        return ins_la_urma(l, x);
    if (!isFull(l))
    {
        l->liber++;// incrementam dimensiunea listei cu 1, adica elementul ce va fi inserat
        for (int i=l->liber;i!=0; i--)
            l->v[i] = l->v[i-1];
        l->v[0] = x;
    }
    return l;
}
//----------------------------------
LISTA populeaza_lista_din_fisier(LISTA l, char* numeFisier)
{
	// if adaugat doar pentru optimizare in cazul in care lista este plina
	// salvam operatii consumatoare de timp: deschidere fisier, citire intreg din fisier, inchidere fisier
	if (ifFull(l))
		return l;
    FILE* f = fopen(numeFisier, "r");
    if (f == NULL)
    {
        printf("\nEroare la deschiderea fisierului. ");
        return l;
    }
    int aux;
    while(fscanf(f, "%d ", &aux) > 0)
        l = ins_la_urma(l, aux);
	// nu mai verificam isFull, intrucat ins_la_urma face aceasta verificare deja
    return l;
}
//------------------------------------------------
LISTA ins_dupa_un_anumit_element(LISTA l, int elementDeInserat, int elementPrecedent)
{
    if (isFull(l) || isEmptyl(l)) return l;

    for (int i=l->liber; i>0; i--)
    {
        if (l->v[i-1] == elementPrecedent)
        {
            l->liber++;
            for (int j=l->liber-1; j!=i; j--)
                l->v[j] = l->v[j-1];
            l->v[i] = elementDeInserat;
            return l;
        }
    }
    printf("Elementul "FORMAT" nu este in lista.", elementPrecedent);
    return l;
}
//-------------------------------------------
int cauta(LISTA l, DATA x)
{
    if (isEmptyl(l)) return -1;
    for (int i=0;i<l->liber;i++)
        if (l->v[i] == x)
            return i;
    return -1;
}
//-------------------------------------------
LISTA elimina_primul(LISTA l)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida!");
        return l;
    }
    for (int i=0; i<l->liber-1; i++)
        l->v[i] = l->v[i+1];
    l->liber--;
    return l;
}
//------------------------------------------
void salveaza_lista_in_fisier(LISTA l, char* numeFisier)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida, nu vom realiza scrierea in fisier.");
        return;
    }
    FILE* f = fopen(numeFisier, "aw");
    if (f == NULL)
    {
        printf("\nEroare la scrierea in fisier.");
        return;
    }
    for (int i=0;i<l->liber;i++)
        fprintf(f, "%d ", l->v[i]);
    fclose(f);
}
//-----------------------------------------
void formeazaListe(LISTA l, LISTA *lpoz, LISTA *lneg)
{
    if (isEmptyl(l))
    {
        printf("\nLista este vida. Operatie imposibila!");
        return;
    }
    for (int i=0;i<l->liber;i++)
        if (l->v[i] >= 0)
            *lpoz=ins_la_urma(*lpoz, l->v[i]);
        else
            *lpoz=ins_la_urma(*lneg, l->v[i]);
}
//-----------------------------------------
LISTA elimina_x(LISTA l, DATA x)
{
    if (isEmptyl(l)) return l;
    for (int i=0;i<l->liber;i++)
        if (l->v[i] == x)
        {
            for (int j=i; j<l->liber-1;j++)
                l->v[j] = l->v[j+1];
            l->liber--;
            return l;
        }
    printf("\nElementul "FORMAT" nu exista in lista.", x);
    return l;
}
